jQuery(document).ready(function($){
/* Expandable features sections */
// set up variables
var featuresRotatorParent = $("#features-engagement");
var featuresCounter = 0;

var featureImageArray = [
	"../img/features/p4-features-engagement-petitions.png",
	"../img/features/p4-features-engagement-events.png",
	"../img/features/p4-features-engagement-meaningful.jpg",
	"../img/features/p4-features-engagement-momentum.jpg"
];
featureImageArray.forEach(function(img){
	featuresCounter++;
	// pre-load the images
    new Image().src = img;
		// create empty divs for bg image crossfading
		featuresRotatorParent.append("<div class='feature-bg feature-bg-"+featuresCounter+"' style='background-image:url("+img+")'></div>");
});

// set up click handlers
$("#features-engagement .expando").on('click', function(){
	var featureNumber = $(this).data('feature');
	if ( !( $(this).hasClass('bg-active') ) ){
		$("#features-engagement .expando").removeClass("bg-active");
		$(this).addClass("bg-active");
		$(".feature-bg").removeClass("active");
		$(".feature-bg-"+featureNumber).addClass("active");
	}
});

});
